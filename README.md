# Proyectouno

####Practicas y ejercicios de gitlab.

`GitLab CI`
Es el hub central de automatización de Gitlab. Es el pedazo de código que nosotros podemos configurar libremente para poder generar las automatizaciones necesarias para que nuestro flujo de trabajo de devops requiera poca o ninguna intervención humana desde el que el desarrollador termina los cambios hasta que llega a produccion.
`Gitlab CI` construye, prueba y despliega cambios continuamente. Esto reduce los riesgos de bugs nuevos o de trabajar versiones no construidas o que no se implementaron.
Toda la configuración de `Gitlab` se hace en un sólo archivo `.gitlab-ci.yml` y se encuentra escrito en lenguaje yamel.

####Variables que se usan en los jobs
- Trigger variables
- Project variables (y protected variables)
- Group variables
- YAML job level variables
- YAML global variables
- Deployment variables
- Pre defined variables